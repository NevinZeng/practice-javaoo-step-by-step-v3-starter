package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {

    private Integer number;

    private Student leader;

    // 用于存储观察者
    private List<Observer> observerList = new ArrayList<>();

    public Klass(Integer number){
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public Student getLeader() {
        return leader;
    }

    public List<Observer> getObserverList() {
        return observerList;
    }

    @Override
    public boolean equals(Object obj) {
        Klass klass = (Klass) obj;
        return this.number.equals(klass.getNumber());
    }

    public void assignLeader(Student student){
        if (student.getKlass() != null && this.equals(student.getKlass())){
            this.leader = student;
            notifyObserver(student);
            return;
        }
        System.out.println("It is not one of us.");
    }

    public Boolean isLeader(Student student){
        return this.leader.equals(student);
    }

    public void attach(Observer observer) {
        observerList.add(observer);
    }

    public void notifyObserver(Student student){
        for (Observer observer : student.getKlass().getObserverList()) {
            observer.update(this, student);
        }
    }
}
