package ooss;

public interface Observer {
    void update(Klass klass, Student student);
}