package ooss;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Teacher extends Person implements Observer {

    private List<Klass> klassList = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduceStr = String.format("My name is %s. I am %d years old. I am a teacher.", this.getName(), this.getAge());
        if (klassList.size() == 0){
            return introduceStr;
        }
        String classString = " I teach Class ";
        classString += klassList.stream().map(klass -> String.valueOf(klass.getNumber())).collect(Collectors.joining(", "));
        introduceStr = introduceStr + classString + ".";
        return introduceStr;
    }

    public void assignTo(Klass klass){
        klassList.add(klass);
    }

    public Boolean belongsTo(Klass klass){
        return klassList.contains(klass);
    }

    public Boolean isTeaching(Student student){
        return klassList.contains(student.getKlass());
    }

    @Override
    public void update(Klass klass, Student student) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.\n",this.getName(), klass.getNumber(), student.getName());
    }
}
