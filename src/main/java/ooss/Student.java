package ooss;

public class Student extends Person implements Observer{

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    @Override
    public String introduce() {
        String introduceStr = String.format("My name is %s. I am %d years old. I am a student.", this.getName(), this.getAge());
        // step 3
        if (klass != null){
            if (klass.getLeader() != null && klass.getLeader().equals(this)){
                introduceStr += " I am the leader of class " + this.klass.getNumber() + ".";
            }else {
                introduceStr += " I am in class " + this.klass.getNumber() + ".";
            }

        }

        // step 5
        return introduceStr;
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public Boolean isIn(Klass klass){
        return this.klass != null && this.klass.equals(klass);
    }

    @Override
    public void update(Klass klass, Student student) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.\n",this.getName(), klass.getNumber(), student.getName());
    }
}
