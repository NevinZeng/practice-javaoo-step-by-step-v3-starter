## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. Learn the importance and precautions of Code Review and learn how to conduct Code Review.

2. Learn Java Stream, master the basic use of stream and common apis, such as filter, map, reduce, etc.

3. Conduct training on java stream to improve your understanding of stream.

4. Learn what object orientation is, including the three features of object orientation.

5. Conduct object-oriented programming training to consolidate object-oriented thinking.

## R (Reflective): Please use one word to express your feelings about today's class.

    Thoughtful.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

    Through today's learning, I have a deeper understanding of the use of stream, in the normal development process can use stream to simplify the code to improve readability.

    It can be seen from the class that the students are very energetic and focused when practicing code, and the effect is better than that of pure theoretical learning. In the future, we can increase the link of typing code in class and let the students participate in it.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

    In the normal development process can use stream to simplify the code to improve readability.I will continue to learn the use of stream.
